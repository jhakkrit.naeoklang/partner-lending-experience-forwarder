package com.example.httpforwardrequest.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URISyntaxException;


@Slf4j
@RestController
@RequestMapping("/api/v1")
public class HTTPProxyController {

    @Value("${x-forward-url.ms-lego-url}")
    private String forwardLegoUrl;

//    @GetMapping(value = "/forward/lego", produces = MediaType.TEXT_HTML_VALUE)
//    public ResponseEntity forwardLego(@RequestParam String accessToken,
//                                      HttpServletResponse response) throws URISyntaxException {
//        URI externalUri = new URI(forwardLegoUrl);
//        HttpHeaders httpHeaders = new HttpHeaders();
//        httpHeaders.setLocation(externalUri);
//        httpHeaders.add(HttpHeaders.SET_COOKIE, String.format("TS01e378eb=%s;Domain=scbone-partner-lending-sit.se.scb.co.th;", accessToken));
//        httpHeaders.add(HttpHeaders.SET_COOKIE, String.format("scbLECOCookie=%s;Domain=scbone-partner-lending-sit.se.scb.co.th;", accessToken));
//        httpHeaders.set("scbLECOCookie", accessToken);
//        return new ResponseEntity<>(httpHeaders, HttpStatus.FOUND);
//    }


    @GetMapping(value = "/forward/lego", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity forwardLego(@RequestParam String accessToken, HttpServletResponse response) throws URISyntaxException, IOException {
        Cookie cookie = new Cookie("scbLECOCookie", accessToken);
        cookie.setMaxAge(-1);
        cookie.setPath("/");
        cookie.setDomain("scbone-partner-lending-sit.se.scb.co.th");
        response.addCookie(cookie);

        cookie = new Cookie("scbLECOCookie2", accessToken);
        cookie.setMaxAge(-1);
        cookie.setPath("/");
        cookie.setDomain("scbone-partner-lending-sit.se.scb.co.th");
        response.addCookie(cookie);

        response.setHeader("scbLECOCookie", accessToken);
        response.sendRedirect(forwardLegoUrl);
        return ResponseEntity.status(HttpStatus.FOUND).build();
    }
}
