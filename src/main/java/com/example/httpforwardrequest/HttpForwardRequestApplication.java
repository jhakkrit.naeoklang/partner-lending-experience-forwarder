package com.example.httpforwardrequest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.retry.annotation.EnableRetry;

//@EnableRetry
@SpringBootApplication
public class HttpForwardRequestApplication {

    public static void main(String[] args) {
        SpringApplication.run(HttpForwardRequestApplication.class, args);
    }

}
